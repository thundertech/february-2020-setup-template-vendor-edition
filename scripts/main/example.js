import { run, click, select } from '../modules/helpers'

/**
 * # Example import
 *
 * Delete or modify this file. We use `[data-hook="name"]` for script selectors.
 *
 */

run('[data-hook="example"]', (element) => {
  click(select(element, '[data-hook="example.button"]'), (e) => {
    select(element, '[data-hook="example.textBlock"]').textContent = `${element.dataset.text}hi there`
  })
})
