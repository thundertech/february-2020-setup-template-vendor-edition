const content = require('./modules/content')
const classLists = require('./modules/classlists')

const data = {
  content,
  classLists,
}

module.exports = data
